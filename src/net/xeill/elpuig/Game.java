package net.xeill.elpuig;

import java.io.*;
import java.util.*;

/**
* This class implements the Game
*
* @author David
*
*/

public class Game {

  int id;
  String title;
  String description;
  float price;

  Game(int id, String title, String description, float price) {
    this.id = id;
    this.title = title;
    this.description = description;
    this.price = price;
  }

  public boolean buy() {
    return false;
  }

  public void print() {
    System.out.println("id: "+id);
    System.out.println("title: "+title);
    System.out.println("description: "+description);
    System.out.println("price: "+price);
  }

}
