package net.xeill.elpuig;

import java.io.*;
import java.util.*;

/**
* This class implements the Code
*
* @author David
*
*/

public class Code {

  int id;
  String title;
  String description;
  float price;

  Code(int id, String title, String description, float price) {
    this.id = id;
    this.title = title;
    this.description = description;
    this.price = price;
  }

  public boolean buy() {
    return false;
  }

}
