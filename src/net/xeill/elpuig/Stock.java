package net.xeill.elpuig;

import java.io.*;
import java.util.*;

/**
* This class implements the Stock
*
* @author David
*
*/

public class Stock {

  ArrayList<Game> games = new ArrayList<Game>();
  ArrayList<Console> consoles = new ArrayList<Console>();
  ArrayList<Code> codes = new ArrayList<Code>();
  Scanner scanner = new Scanner(System.in);

  Stock() {
    initFakeStock();
  }

  public void initFakeStock() {

    // Juegos
    games.add(new Game(1, "Fornite", "Juego de supervivencia", 10.00f));

    // Consolas
    consoles.add(new Console(1, "Switch", "Nintendo", 300.00f));

    // Codes
    codes.add(new Code(1, "Pase de batalla", "Pase de batalla temporada 2.2", 10.00f));

  }
  public void addUsuarios() {
    File archivo;
    FileWriter escribir;
    PrintWriter linea;
    String nombre="";
    String email="";
    String apellido="";
    int id=0;
    IO io = new IO();
    archivo = new File("Usuarios.txt");
    if (!archivo.exists()){
    try{
    archivo.createNewFile();

    System.out.println("Ingresa un nuevo id");

    id = scanner.nextInt();

    System.out.println("Ingresa tu Nombre");
     nombre = scanner.next();

     System.out.println("Ingresa tu Apellido");
      apellido = scanner.next();

    System.out.println("Ingresa tu Email");
     email = scanner.next();

    escribir = new FileWriter(archivo,true);
    linea = new PrintWriter(escribir);

    linea.print("Id: "+id+"\t");
    linea.print("Nombre: "+nombre+"\t");
    linea.print("Apellido: "+apellido+"\t");
    linea.print("Email: "+email+"\n");
    linea.close();

    } catch(Exception e){
    io.welcome();
    }
    }else{
try {

  System.out.println("Ingresa un nuevo id");

  id = scanner.nextInt();

  System.out.println("Ingresa tu Nombre");
   nombre = scanner.next();

   System.out.println("Ingresa tu Apellido");
    apellido = scanner.next();

  System.out.println("Ingresa tu Email");
   email = scanner.next();

  escribir = new FileWriter(archivo,true);
  linea = new PrintWriter(escribir);

  linea.print("Id: "+id+"\t ");
  linea.print("Nombre: "+nombre+"\t ");
  linea.print("Apellido: "+apellido+"\t ");
  linea.print("Email: "+email+"\n");
  linea.close();

  } catch(Exception e){
  io.welcome();
  }
}
    }
    public void addJuegos() {
      File archivo;
      FileWriter escribir;
      PrintWriter linea;
      String title="";
      String descripcion="";
      int id=0;
      float precio=0;
      IO io = new IO();
      archivo = new File("Juegos.txt");
      if (!archivo.exists()){
      try{
      archivo.createNewFile();

      System.out.println("Ingresa un nuevo id");

      id = scanner.nextInt();

      System.out.println("Ingresa un Title");
       title = scanner.next();

       System.out.println("Ingresa una Categoria");
        descripcion = scanner.next();

      System.out.println("Ingresa un Precio");
       precio = scanner.nextFloat();

      escribir = new FileWriter(archivo,true);
      linea = new PrintWriter(escribir);

      linea.print("Id: "+id+"\t");
      linea.print("Title: "+title+"\t");
      linea.print("Categoria: "+descripcion+"\t");
      linea.print("Precio: "+precio+"\n");
      linea.close();

      } catch(Exception e){
      io.welcome();
      }
      }else{
  try {
    System.out.println("Ingresa un nuevo id");

    id = scanner.nextInt();

    System.out.println("Ingresa un Title");
     title = scanner.next();

     System.out.println("Ingresa una Categoria");
      descripcion = scanner.next();

    System.out.println("Ingresa un Precio");
     precio = scanner.nextFloat();

    escribir = new FileWriter(archivo,true);
    linea = new PrintWriter(escribir);

    linea.print("Id: "+id+"\t");
    linea.print("Title: "+title+"\t");
    linea.print("Categoria: "+descripcion+"\t");
    linea.print("Precio: "+precio+"\n");
    linea.close();

    } catch(Exception e){
    io.welcome();
    }
  }
      }
      public void addCodigos() {
        File archivo;
        FileWriter escribir;
        PrintWriter linea;
        String title="";
        String descripcion="";
        int id=0;
        float precio=0;
        IO io = new IO();
        archivo = new File("Codigos.txt");
        if (!archivo.exists()){
        try{
        archivo.createNewFile();

        System.out.println("Ingresa un nuevo id");

        id = scanner.nextInt();

        System.out.println("Ingresa un Title");
         title = scanner.next();

         System.out.println("Ingresa una Categoria");
          descripcion = scanner.next();

        System.out.println("Ingresa un Precio");
         precio = scanner.nextFloat();

        escribir = new FileWriter(archivo,true);
        linea = new PrintWriter(escribir);

        linea.print("Id: "+id+"\t");
        linea.print("Title: "+title+"\t");
        linea.print("Categoria: "+descripcion+"\t");
        linea.print("Precio: "+precio+"\n");
        linea.close();

        } catch(Exception e){
        io.welcome();
        }
        }else{
    try {
      System.out.println("Ingresa un nuevo id");

      id = scanner.nextInt();

      System.out.println("Ingresa un Title");
       title = scanner.next();

       System.out.println("Ingresa una Categoria");
        descripcion = scanner.next();

      System.out.println("Ingresa un Precio");
       precio = scanner.nextFloat();

      escribir = new FileWriter(archivo,true);
      linea = new PrintWriter(escribir);

      linea.print("Id: "+id+"\t");
      linea.print("Title: "+title+"\t");
      linea.print("Categoria: "+descripcion+"\t");
      linea.print("Precio: "+precio+"\n");
      linea.close();

      } catch(Exception e){
      io.welcome();
      }
    }
        }
        public void PedidoCodigo() {
          File archivo;
          FileWriter escribir;
          PrintWriter linea;
          String title="";
          String descripcion="";
          int id=0;
          float precio=0;
          IO io = new IO();
          archivo = new File("Pedidos.txt");
          if (!archivo.exists()){
          try{
          archivo.createNewFile();

          System.out.println("Ingresa El Nombre del Codigo");
           title = scanner.next();

           System.out.println("Ingresa una Categoria");
            descripcion = scanner.next();

          System.out.println("Ingresa tu Id de usuario.");
           id = scanner.nextInt();

          escribir = new FileWriter(archivo,true);
          linea = new PrintWriter(escribir);
          linea.print("Producto: Codigo"+"\n");
          linea.print("Nombre: "+title+"\t");
          linea.print("Categoria: "+descripcion+"\t");
          linea.print("Id de usuario: "+id+"\n");
          linea.close();

          } catch(Exception e){
          io.welcome();
          }
          }else{
            try{
            System.out.println("Ingresa El Nombre del Codigo");
             title = scanner.next();

             System.out.println("Ingresa una Categoria");
              descripcion = scanner.next();

            System.out.println("Ingresa tu Id de usuario.");
             id = scanner.nextInt();

            escribir = new FileWriter(archivo,true);
            linea = new PrintWriter(escribir);
            linea.print("Producto: Codigo"+"\n");
            linea.print("Nombre: "+title+"\t");
            linea.print("Categoria: "+descripcion+"\t");
            linea.print("Id de usuario: "+id+"\n");
            linea.close();

            } catch(Exception e){
            io.welcome();
            }
      }
          }
          public void PedidoJuego() {
            File archivo;
            FileWriter escribir;
            PrintWriter linea;
            String title="";
            String descripcion="";
            int id=0;
            float precio=0;
            IO io = new IO();
            archivo = new File("Pedidos.txt");
            if (!archivo.exists()){
            try{
            archivo.createNewFile();

            System.out.println("Ingresa El Nombre del Juego");
             title = scanner.next();

             System.out.println("Ingresa una Categoria");
              descripcion = scanner.next();

            System.out.println("Ingresa tu Id de usuario.");
             id = scanner.nextInt();

            escribir = new FileWriter(archivo,true);
            linea = new PrintWriter(escribir);
            linea.print("Producto: Juego"+"\n");
            linea.print("Nombre: "+title+"\t");
            linea.print("Categoria: "+descripcion+"\t");
            linea.print("Id de usuario: "+id+"\n");
            linea.close();

            } catch(Exception e){
            io.welcome();
            }
            }else{
              try{
              System.out.println("Ingresa El Nombre del Juego");
               title = scanner.next();

               System.out.println("Ingresa una Categoria");
                descripcion = scanner.next();

              System.out.println("Ingresa tu Id de usuario.");
               id = scanner.nextInt();

              escribir = new FileWriter(archivo,true);
              linea = new PrintWriter(escribir);
              linea.print("Producto: Juego"+"\n");
              linea.print("Nombre: "+title+"\t");
              linea.print("Categoria: "+descripcion+"\t");
              linea.print("Id de usuario: "+id+"\n");
              linea.close();

              } catch(Exception e){
              io.welcome();
              }
        }
            }
          public void PedidoConsola() {
            File archivo;
            FileWriter escribir;
            PrintWriter linea;
            String title="";
            String descripcion="";
            int id=0;
            float precio=0;
            IO io = new IO();
            archivo = new File("Pedidos.txt");
            if (!archivo.exists()){
            try{
            archivo.createNewFile();

            System.out.println("Ingresa El Nombre de la Consola");
             title = scanner.next();

             System.out.println("Ingresa una Compañia");
              descripcion = scanner.next();

            System.out.println("Ingresa tu Id de usuario.");
             id = scanner.nextInt();

            escribir = new FileWriter(archivo,true);
            linea = new PrintWriter(escribir);
            linea.print("Producto: Consola"+"\n");
            linea.print("Nombre: "+title+"\t");
            linea.print("Compañia: "+descripcion+"\t");
            linea.print("Id de usuario: "+id+"\n");
            linea.close();

            } catch(Exception e){
            io.welcome();
            }
            }else{
              try{
              System.out.println("Ingresa El Nombre de la Consola");
               title = scanner.next();

               System.out.println("Ingresa una Compañia");
                descripcion = scanner.next();

              System.out.println("Ingresa tu Id de usuario.");
               id = scanner.nextInt();

              escribir = new FileWriter(archivo,true);
              linea = new PrintWriter(escribir);
              linea.print("Producto: Consola"+"\n");
              linea.print("Nombre: "+title+"\t");
              linea.print("Compañia: "+descripcion+"\t");
              linea.print("Id de usuario: "+id+"\n");
              linea.close();

              } catch(Exception e){
              io.welcome();
              }
        }
            }
        public void addConsolas() {
          File archivo;
          FileWriter escribir;
          PrintWriter linea;
          String title="";
          String descripcion="";
          int id=0;
          float precio=0;
          IO io = new IO();
          archivo = new File("Consolas.txt");
          if (!archivo.exists()){
          try{
          archivo.createNewFile();

          System.out.println("Ingresa un nuevo id");

          id = scanner.nextInt();

          System.out.println("Ingresa un Nombre");
           title = scanner.next();

           System.out.println("Ingresa una Compañia");
            descripcion = scanner.next();

          System.out.println("Ingresa un Precio");
           precio = scanner.nextFloat();

          escribir = new FileWriter(archivo,true);
          linea = new PrintWriter(escribir);

          linea.print("Id: "+id+"\t");
          linea.print("Title: "+title+"\t");
          linea.print("Categoria: "+descripcion+"\t");
          linea.print("Precio: "+precio+"\n");
          linea.close();

          } catch(Exception e){
          io.welcome();
          }
          }else{
      try {
        System.out.println("Ingresa un nuevo id");

        id = scanner.nextInt();

        System.out.println("Ingresa un Nombre");
         title = scanner.next();

         System.out.println("Ingresa una Compañia");
          descripcion = scanner.next();

        System.out.println("Ingresa un Precio");
         precio = scanner.nextFloat();

        escribir = new FileWriter(archivo,true);
        linea = new PrintWriter(escribir);

        linea.print("Id: "+id+"\t");
        linea.print("Title: "+title+"\t");
        linea.print("Categoria: "+descripcion+"\t");
        linea.print("Precio: "+precio+"\n");
        linea.close();

        } catch(Exception e){
        io.welcome();
        }
      }
          }


public void leer (String Archivo){
  try {

  FileReader r = new FileReader (Archivo);
  BufferedReader buffer = new BufferedReader (r);

  String temp="";

  while (temp!=null){
  temp= buffer.readLine();

  if(temp==null)
  break;
  System.out.println(temp);
  }

  }catch(Exception e){
  System.out.println("No existe el archivo.");
  }

}


  }


/*  public void imprimirJuegos() {

  }
    public void imprimirConsolas() {

}

      public void imprimirCodigos() {

      }
      public void imprimirUsers() {

      }
      */
