package net.xeill.elpuig;

import java.io.*;
import java.util.*;

/**
* This class implements the Console
*
* @author David
*
*/

public class Console {

  int id;
  String name;
  String company;
  float price;

  Console(int id, String name, String company, float price) {
    this.id = id;
    this.name = name;
    this.company = company;
    this.price = price;
  }

  public boolean buy() {
    return false;
  }

}
