package net.xeill.elpuig;

import java.io.*;
import java.util.*;

/**
* This class implements the User
*
* @author David
*
*/

public class User {

  int id;
  String name;
  String surname;
  String email;
  ArrayList<Console> consolas;
  ArrayList<Code> codigos;
  ArrayList<Game> juegos;
  User(int id, String name, String surname, String email) {
    this.id = id;
    this.name = name;
    this.surname = surname;
    this.email = email;
    this.codigos = new ArrayList<Code>();
    this.consolas = new ArrayList<Console>();
    this.juegos = new ArrayList<Game>();
  }

/*  boolean buyGame(Game g) {
    this.juegos.add(g);
    return true;
  }

  boolean buyConsole(Console c) {
    this.consolas.add(c);
    return true;
  }
  boolean buyCode(Code cd) {
    this.codigos.add(cd);
    return true;
  }
*/
}
