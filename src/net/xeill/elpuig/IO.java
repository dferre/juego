package net.xeill.elpuig;

import java.io.*;
import java.util.*;

/**
* This class implements the IO - Input/Output
*
* @author David
*
*/

public class IO {

  Scanner scanner = new Scanner(System.in);
  /**
  * Clears the screen.
  */
  void cls() {
    System.out.print("\033\143");
  }

  public void welcome() {
    System.out.println("------- Bienvenido a la tienda de videojuegos -------");
    System.out.println("1 - Usuarios");
    System.out.println("2 - Juegos");
    System.out.println("3 - Consolas");
    System.out.println("4 - Códigos");
    System.out.println("5 - Mostrar Pedidos");
    System.out.println("6 - Salir");
    System.out.println("NOTA: LOS ESPACIOS SE REPRESENTAN CON UN '_' ");
  }

  public int getWelcome() {
    String soption;
    int option;
    try {
      soption = scanner.nextLine();
      option = Integer.parseInt(soption);
    } catch (Exception e) {
      cls();
      welcome();
       return getWelcome();
    }
    return option;
  }


  void usuarios() {
    System.out.println("------- Usuarios -------");
    System.out.println("1 - Mostrar usuarios");
    System.out.println("2 - Añadir usuario");
  }

  public int getUsuariosOpcion() {
    String soption;
    int option;
    try {
      soption = scanner.nextLine();
      option = Integer.parseInt(soption);
    } catch (Exception e) {
      cls();
      usuarios();
       return getUsuariosOpcion();
    }
    return option;
  }




  void juegos() {
    System.out.println("------- Juegos -------");
    System.out.println("1 - Mostrar juegos");
    System.out.println("2 - Añadir juegos");
      System.out.println("3 - Pedir juego");
    // en el 2 mostrar menu : usuario que compra y quitar del arraylist
   }

   public int getJuegosOpcion() {
     String soption;
     int option;

     try {
       soption = scanner.nextLine();
       option = Integer.parseInt(soption);
     } catch (Exception e) {
       cls();
       juegos();
        return getJuegosOpcion();
     }
     return option;
   }

   public int addJuegos() {
       ArrayList<Game> Games = new ArrayList<Game>();
       int id = scanner.nextInt();
       scanner.nextLine();
       String title = scanner.nextLine();
       String description = scanner.nextLine();
       float price = scanner.nextFloat();
     try {
       Games.add(new Game(id, title, description, price));
     } catch (Exception e) {
       cls();
       juegos();

     }
       return getJuegosOpcion();
   }

  void consolas() {
    System.out.println("------- Consolas -------");
    System.out.println("1 - Mostrar Consolas");
    System.out.println("2 - Añadir Consolas");
      System.out.println("3 - Pedir Consola");
 }

 public int getConsolasOpcion() {
   String soption;
   int option;
   try {
     soption = scanner.nextLine();
     option = Integer.parseInt(soption);
   } catch (Exception e) {
     cls();
     consolas();
      return getConsolasOpcion();
   }
   return option;
 }

 public int addConsolas() {
     ArrayList<Console> Consoles = new ArrayList<Console>();
     int id = scanner.nextInt();
     scanner.nextLine();
     String name = scanner.nextLine();
     String company = scanner.nextLine();
     float price = scanner.nextFloat();
   try {
     Consoles.add(new Console(id, name, company, price));
   } catch (Exception e) {
     cls();
     consolas();

   }
   return getConsolasOpcion();
 }

  void codigos() {
    System.out.println("------- Códigos -------");
    System.out.println("1 - Mostrar Códigos");
    System.out.println("2 - Añadir Códigos");
      System.out.println("3 - Pedir Codigo");
  }
  public int getCodigosOpcion() {
    String soption;
    int option;
    try {
      soption = scanner.nextLine();
      option = Integer.parseInt(soption);
    } catch (Exception e) {
      cls();
      codigos();

    }
    return getCodigosOpcion();
  }
  public int addCodigos() {
      ArrayList<Code> Codes = new ArrayList<Code>();
      int id = scanner.nextInt();
      scanner.nextLine();
      String title = scanner.nextLine();
      String description = scanner.nextLine();
      float price = scanner.nextFloat();
    try {
      Codes.add(new Code(id, title, description, price));
    } catch (Exception e) {
      cls();
      codigos();

    }
           return getCodigosOpcion();
  }
   void salir() {

System.exit(1);
    }


}
