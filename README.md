# video-game-store
Video Game Store, Java Terminal

## How to compile?
```bash
# Go into the project source folder (src)
$ cd video-game-store/src

# Compile the main class
$ javac net/xeill/elpuig/Main.java

# Run the main
$ java net.xeill.elpuig.Main
```
# Este juego son unos menus de una tienda de videojuegos.
# Cada vez que alguien hace un pedido o quiere añadir un producto al stock, se añaden en los diferentes documentos.
